package omnom;

public class Haustier {

	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;

	public Haustier() {

	}

	public Haustier(String name) {

		this.name = name;
		this.hunger = 100;
		this.muede = 100;
		this.gesund = 100;
		this.zufrieden = 100;

	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {

		if (hunger >= 0 || hunger <= 100)
			this.hunger = hunger;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {

		if (muede >= 0 || muede <= 100)
			this.muede = muede;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {

		if (zufrieden < 0)
			this.zufrieden = 0;
		else if (zufrieden > 100)
			this.zufrieden = 100;
		else
			this.zufrieden = zufrieden;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {

		if (gesund >= 0 || gesund <= 100)
			this.gesund = gesund;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void fuettern(int hunger) {
		this.setHunger(hunger + this.getHunger());
	}

	public void schlafen(int dauer) {
		this.setMuede(dauer + this.getMuede());
	}

	public void spielen(int dauer) {
		if (zufrieden + dauer < 0)
			this.zufrieden = 0;
		else if (zufrieden + dauer > 100)
			this.zufrieden = 100;
		else
			this.zufrieden = this.zufrieden + dauer;

	}

	public void heilen() {
		this.gesund = 100;
	}

}
