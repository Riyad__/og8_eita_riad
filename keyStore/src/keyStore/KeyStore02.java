package keyStore;

import java.util.ArrayList;

public class KeyStore02 {

	ArrayList<String> arrayList = new ArrayList<String>();

	public boolean add(String eintrag) {
		return arrayList.add(eintrag);
	}

	public int indexOf(String eingabe) {
		return arrayList.indexOf(eingabe);
	}

	public boolean remove(int index) {
		return  arrayList.remove(index) != null;
	}

	@Override
	public String toString() {
		return "KeyStore02 [arrayList=" + arrayList + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
