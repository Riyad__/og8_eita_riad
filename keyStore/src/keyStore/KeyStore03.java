package keyStore;

import java.util.LinkedList;

public class KeyStore03 {

	LinkedList<String> liste = new LinkedList<String>();

	public boolean add(String eintrag) {
		return liste.add(eintrag);
	}

	public int indexOf(String eingabe) {
		return liste.indexOf(eingabe);
	}

	public boolean remove(int index) {
		return liste.remove(index) != null;
	}

	@Override
	public String toString() {
		return "KeyStore03 [liste=" + liste + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	

}
