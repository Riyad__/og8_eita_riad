package keyStore;

import java.util.Arrays;

public class KeyStore01 {
	String[] meinArray = new String[1];
	int counter = -1;

	public KeyStore01() {
	}

	public boolean add(String eintrag) {
		counter++;
		if (counter < meinArray.length) {
			this.meinArray[counter] = eintrag;
			return true;
		} else {
			String[] Array = new String[meinArray.length + 1];
			for (int i = 0; i < meinArray.length; i++) {
				Array[i] = meinArray[i];
			}
			Array[counter] = eintrag;

			meinArray = Array;
			this.meinArray[counter] = eintrag;
		}
		return false;
	}

	public int indexOf(String eingabe) {
		for (int i = 0; i < meinArray.length; i++)
			if (meinArray[i].equals(eingabe)) {
				return i;
			}
		return -1;
	}

	public boolean remove(int index) {
		for (int i = index; i < meinArray.length - 1; i++) {
			meinArray[i] = meinArray[i + 1];
		}
		counter--;
		return true;
	}

	@Override
	public String toString() {

		return "KeyStore01 [meinArray=" + Arrays.toString(meinArray) + ", counter=" + counter + "]";
	}

}
