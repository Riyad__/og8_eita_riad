package zooTycoon;

public class Addon {

	private int idNummer;
	private String bezeichnung;
	private double verkaufspreis;
	private int bestandist;
	private int bestandMax;

	public Addon(int idNummer, String bezeichnung, double verkaufspreis, int bestandist, int bestandMax) {
		this.idNummer = idNummer;
		this.bezeichnung = bezeichnung;
		this.verkaufspreis = verkaufspreis;
		this.bestandist = bestandist;
		this.bestandMax = bestandMax;
	}
	
	public Addon () {}

	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getBestandist() {
		return bestandist;
	}

	public void setBestandist(int bestandist) {
		this.bestandist = bestandist;
	}

	public double getVerkaufspreis() {
		return verkaufspreis;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public int getBestandMax() {
		return bestandMax;
	}

	public void setBestandMax(int bestandMax) {
		this.bestandMax = bestandMax;
	}

	public void aendereBestand(int anzahl) {

	}

	public double berechneGesamtwert() {
		return 0;
	}

}
