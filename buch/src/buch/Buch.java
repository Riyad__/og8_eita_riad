package buch;

public class Buch implements Comparable<Buch>  {
String author,titel,isban;

public Buch(String author, String titel, String isban) {
	super();
	this.author = author;
	this.titel = titel;
	this.isban = isban;
	
}

public String getAuthor() {
	return author;
}

public void setAuthor(String author) {
	this.author = author;
}

public String getTitel() {
	return titel;
}

public void setTitel(String titel) {
	this.titel = titel;
}

public String getIsban() {
	return isban;
}

public void setIsban(String isban) {
	this.isban = isban;
}
public boolean equals(Buch o){
	return o. getIsban() .equals(this.isban);
}



@Override
public String toString() {
	return "Buch [author=" + author + ", titel=" + titel + ", isban=" + isban + "]";
}

public int compareTo(Buch o){
	return this.isban.compareTo(o.getIsban());
	
}

public int compareTo1(Buch o) {
	// TODO Auto-generated method stub
	return 0;
}
}