package kontrollstrukturen;

import java.util.Scanner;

public class Primzahl {
	public static boolean istPrim(long zahl) {
		Stoppuhr uhr = new Stoppuhr();
		uhr.start();
		if (zahl < 2) {
			return false;
		}

		for (long i = 2; i < zahl; i++) {
			if (zahl % i == 0) {
				return false;
			}

		}

		uhr.stopp();
		System.out.println(uhr.getDauerInMs());
		return true;

	}

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		Stoppuhr uhr = new Stoppuhr();
		System.out.println("Geben Sie eine Zahl ein: ");
		// long prim = 9_999_999_929L;
		while (true) {
			long prim = eingabe.nextLong();

			if (istPrim(prim)) {
				System.out.println(prim + " ist eine Primzahl!");
			} else {
				System.out.println(prim + " ist keine Primzahl");

				System.out.println(uhr.getDauerInMs());
			}
		}
	}
}
