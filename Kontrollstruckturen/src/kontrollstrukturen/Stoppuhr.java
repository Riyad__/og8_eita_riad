package kontrollstrukturen;

public class Stoppuhr {
	private long startzeit;
	private long endzeit;

	public void start() {
		startzeit = System.currentTimeMillis();

	}

	public void stopp() {
		endzeit = System.currentTimeMillis();

	}

	public void reset() {

	}

	public long getStartzeit() {
		return startzeit;
	}

	public void setStartzeit(long startzeit) {
		this.startzeit = startzeit;
	}

	public long getEndzeit() {
		return endzeit;
	}

	public void setEndzeit(long endzeit) {
		this.endzeit = endzeit;
	}
	public long getDauerInMs() {
		long dauer=endzeit-startzeit;
		return dauer;
		

}
}