package kontrollstrukturen;

import java.util.Scanner;

public class BMI {

	private static Scanner eingabe;

	public static void main(String[] args) {
		eingabe = new Scanner(System.in);
		System.out.println("Welches Gewicht?");
		double gewicht = eingabe.nextInt();
		System.out.println("Welche Grosse?");
		double grosse = eingabe.nextInt();
		grosse = grosse / 100;
		System.out.println("Welches Geschlecht (M=1 oder W=0 )");

		double geschlecht = eingabe.nextInt();

		double bmi;

		bmi = gewicht / (grosse * grosse);

		if (geschlecht == 1) {
			if (bmi > 0 && bmi < 20) {
				System.out.println("BMI" + bmi + " = Untergewicht");
			} else if (bmi > 19 && bmi < 25) {
				System.out.println("BMI: " + bmi + " = Normalgewicht");
			} else if (bmi > 25) {
				System.out.println("BMI: " + bmi + " = Ubergewicht");
			}
		} else if (geschlecht == 0) {
			if (bmi > 0 && bmi < 19) {
				System.out.println("BMI: " + bmi + " = Untergewicht");
			} else if (bmi > 19 && bmi < 24) {
				System.out.println("BMI: " + bmi + " = Normalgewicht");
			} else if (bmi > 19 && bmi < 24) {
				System.out.println("BMI: " + bmi + " = Normalgewicht");
			} else if (bmi > 24) {
				System.out.println("BMI: " + bmi + " = Ubergewicht");
			}
		}
	} // end of main
} // end of class BMI
