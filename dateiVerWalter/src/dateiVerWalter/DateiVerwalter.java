package dateiVerWalter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DateiVerwalter {
	private File file;

	public DateiVerwalter(File file) {
		this.file = file;
	}

	public void lesen() {
	//	ghhhh
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
		} catch (Exception e) {
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
	}

	public void schreibe(int primzahlen) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(primzahlen + "");
			bw.newLine();
			bw.flush();

		} catch (IOException e) {
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		File file = new File("OG8.txt");
		DateiVerwalter dv = new DateiVerwalter(file);
		// dv.schreibe("Filip");
		dv.lesen();
	}

}
