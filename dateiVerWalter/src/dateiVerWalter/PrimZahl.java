package dateiVerWalter;

import java.io.File;
import java.util.Arrays;

import javax.swing.text.StyledEditorKit.ForegroundAction;

public class PrimZahl {
	public static void main(String[] args) {
		File file = new File("prim.txt");
		DateiVerwalter dv = new DateiVerwalter(file);

		boolean[] primzahlen = berechnePrimzahlen(10000000);
		for (int i = 0; i < primzahlen.length; i++) {

			if (primzahlen[i]) {

				System.out.println(i);
				dv.schreibe(i);

			}

		}

		dv.lesen();
	}

	public static boolean[] berechnePrimzahlen(int obergrenze) {
		boolean[] primzahlen = new boolean[obergrenze + 1];
		Arrays.fill(primzahlen, Boolean.TRUE); // TRUE bedeutet, dass diese Position eine Primzahl ist
		primzahlen[0] = false; // 0 ausschließen
		primzahlen[1] = false; // 1 ausschließen
		// Alle möglichen Teiler durchlaufen
		for (int i = 2; i <= obergrenze; i++) {
			int momentanerWert = i; // Anfangs Wert von i, später Vielfaches von i
			if (primzahlen[momentanerWert]) {
				momentanerWert += i;
				while (momentanerWert <= obergrenze) {
					primzahlen[momentanerWert] = false;
					momentanerWert += i; // Momentanen Wert um i erhöhen
				}
			}
		}
		return primzahlen; // Feld zurückgeben
	}
}