package de.promocode.logic;



public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int findPromoCode(long[] list, long searchValue) {
		
		
		
		int low = 0;
		int high = list.length - 1;
		while (low <= high) {
			
			int center = (low + high) / 2;
			if (searchValue == list[center]) {
				
			
				return center;

			} else {

				if (searchValue < list[center]) {
					high = center - 1;
				} else {
					low = center + 1;

				}
			}

		}
		
		return NOT_FOUND;
	}

}
