package de.futurehome.tanksimulator;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class TankSimulator extends Frame {
	
	public Tank myTank;
	
	private Label lblUeberschrift = new Label("Tank-Simulator");
	public  Label lblFuellstand = new Label("______");
	public  Label lblSilder = new Label("");

	
	public Button btnBeenden = new Button("Beenden");
	public Button btnEinfuellen = new Button("Einfüllen");
	public Button btnVerbrauchen = new Button("Verbrauchen");
	public Button btnZur�cksetzen = new Button("Zur�cksetzen");
	
	private Panel pnlNorth = new Panel();
	private Panel pnlCenter = new Panel(new FlowLayout());
	private Panel pnlSouth = new Panel(new GridLayout(1, 0));

	private MyActionListener myActionListener = new MyActionListener(this);
	public JProgressBar progressBar = new JProgressBar();
	private final JPanel panel = new JPanel();
	public JSlider slider = new JSlider();
	

	public TankSimulator() {
		super("Tank-Simulator");
		
		myTank = new Tank(0);
		
		this.lblUeberschrift.setFont(new Font("", Font.BOLD, 16));
		this.pnlNorth.add(this.lblUeberschrift);
		pnlCenter.add(slider);
		slider.setMajorTickSpacing(1);
		slider.setMaximum(4);
		slider.setBackground(Color.MAGENTA);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBorder(UIManager.getBorder("ComboBox.border"));
		slider.setFont(UIManager.getFont("CheckBoxMenuItem.acceleratorFont"));
		slider.setMinimum(1);
		slider.setValue(0);
		pnlCenter.add(panel);
		this.pnlSouth.add(this.btnEinfuellen);
		pnlSouth.add(btnVerbrauchen);
		this.btnVerbrauchen.addActionListener(myActionListener);
		this.pnlSouth.add(this.btnBeenden);
		this.pnlSouth.add(this.btnZur�cksetzen);
		this.add(this.pnlNorth, BorderLayout.NORTH);
		this.add(this.pnlCenter, BorderLayout.CENTER);
		this.pnlCenter.add(this.lblFuellstand);
		progressBar.setBackground(Color.GREEN);
		progressBar.setStringPainted(true);
		
			pnlCenter.add(progressBar);
		this.add(this.pnlSouth, BorderLayout.SOUTH);
		this.pack();
		this.setVisible(true);
		
		// Ereignissteuerung
		this.btnEinfuellen.addActionListener(myActionListener);
		this.btnBeenden.addActionListener(myActionListener);
		this.btnZur�cksetzen.addActionListener(myActionListener);
	}

	public static void main(String argv[]) {
		TankSimulator f = new TankSimulator();
	}
}