package algorithmen;

import java.util.Arrays;

public class AlgorithmusSuche {
	private static final int NICHT_GEFUNDEN = 0;

	public static void main(String[] args) {

		long[] codes = new long[19000000];
		for (long i = 0; i < codes.length; i++) {
			codes[(int) i] = i;
		}
		System.out.println("______________________________");
		System.out.println("Best Case:" + lineareSearch(0, codes));
		System.out.println("______________________________");
		System.out.println("Average Case" + lineareSearch(9499999, codes));
		System.out.println("______________________________");
		System.out.println("Worst Case" + lineareSearch(19000000, codes));
		System.out.println("______________________________");
		System.out.println("______________________________");
		System.out.println("______________________________");
		System.out.println("Best Case:" + BinarySearch(9499999, codes));
		System.out.println("______________________________");
		System.out.println("Average Case:" + BinarySearch(626, codes));
		System.out.println("______________________________");
		System.out.println("Worst Case:" + BinarySearch(19000000, codes));

	}

	public static boolean lineareSearch(long traget, long[] array) {
		Stoppuhr uhr = new Stoppuhr();
		uhr.start();
		System.out.println("Lineare Suche nach" + traget);
		int stepcounter = 0;
		for (long i = 0; i < array.length; i++) {
			stepcounter++;
			if (i == traget) {
				uhr.stopp();
				System.out.println("Schrittanzahl: " + stepcounter + " Die gebrauchte Zeit f�r die Berechnung sind: "
						+ uhr.getDauerInMs() + "ms");

				return true;

			}

		}
		uhr.stopp();
		System.out.println("Schrittanzahl: " + stepcounter + " Die gebrauchte Zeit f�r die Berechnung sind: "
				+ uhr.getDauerInMs() + "ms");
		uhr.stopp();

		return false;
	}

	public static boolean BinarySearch(long traget, long[] array) {
		Stoppuhr uhr = new Stoppuhr();
		uhr.start();
		System.out.println("Binaere Suche nach " + traget);
		int stepcounter = 0;
		int low = 0;
		int high = array.length - 1;
		while (low <= high) {
			stepcounter++;
			int center = (low + high) / 2;
			if (traget == array[center]) {
				uhr.stopp();
				System.out.println("Schrittanzahl: " + stepcounter + " Die gebrauchte Zeit f�r die Berechnung sind: "
						+ uhr.getDauerInMs() + "ms");

				return true;

			} else {

				if (traget < array[center]) {
					high = center - 1;
				} else {
					low = center + 1;

				}
			}

		}
		uhr.stopp();

		System.out.println("Schrittanzahl: " + stepcounter + " Die gebrauchte Zeit f�r die Berechnung sind: "
				+ uhr.getDauerInMs() + "ms");
		return false;
	}
}