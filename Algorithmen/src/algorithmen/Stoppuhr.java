package algorithmen;

public class Stoppuhr {
	private long startzeit;
	private long endzeit;

	public void start() {
		startzeit = System.currentTimeMillis();

	}

	public void stopp() {
		endzeit = System.currentTimeMillis();

	}

	public void reset() {
		startzeit = 0;
		endzeit = 0;

	}

	public long getDauerInMs() {
		long dauer = endzeit - startzeit;
		return dauer;

	}
}
