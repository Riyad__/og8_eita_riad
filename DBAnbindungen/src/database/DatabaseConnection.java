package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseConnection {

    private String driver = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://localhost/velocèbello";
    private String user = "root";
    private String password = "";

    public boolean InsertMaterialien (String P_material_id, String  einzelpreis, String bezeichnung) {
        String sql = "INSERT INTO t_materialien ( P_material_id , einzelpreis, bezeichnung) VALUES ('" +P_material_id + "', '" + einzelpreis + "', '" + bezeichnung + "')";

        try {
            //JDBC-Treiber Laden
            Class.forName(driver);

            Connection con = DriverManager.getConnection(url, user, password);

            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        DatabaseConnection dbc = new DatabaseConnection();
        dbc.InsertMaterialien("12", "223","rrrr");
    }
}