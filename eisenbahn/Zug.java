package eisenbahn;

import java.util.Arrays;

public class Zug {
	private String zugnr;
	private Lok lok;
	private Waggon[] waggon;
	
	public Zug(String zugnr, Lok lok) {
		super();
		this.zugnr = zugnr;
		this.setLok(lok);
		this.waggon = new Waggon[5];
	}

	public String getZugnr() {
		return zugnr;
	}
	public void setZugnr(String zugnr) {
		this.zugnr = zugnr;
	}
	public Lok getLok() {
		return lok;
	}
	public void setLok(Lok lok) {
		if(lok != null) {
			this.lok = lok;
			this.lok.setZug(this);
		}
	}
	
	public Waggon[] getWaggon() {
		return waggon;
	}

	public void setWaggon(Waggon[] waggon) {
		this.waggon = waggon;
	}

	@Override
	public String toString() {
		return "Zug [zugnr=" + zugnr + ", lok=" + lok + ", waggon=" + Arrays.toString(waggon) + "]";
	}
}
