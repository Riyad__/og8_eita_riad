import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class KindGenerator {

	// Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den
	// Daten eine Liste an Kind-Objekte anlegen
	public static Kind[] generateKinderListe() {
		Kind kinder[] = new Kind[10000];

		File file = new File("kinddaten.txt");

		if (!file.canRead() || !file.isFile())
			System.exit(0);

		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(file));
			String zeile = "'";
			int counter = 0;

			while ((zeile = in.readLine()) != null) {

				Kind kind = new Kind();
				String newzeile = zeile.replaceAll(",", "");

				String[] parts = newzeile.split(" ");

				kind.vorname = parts[0];
				kind.nachname = parts[1];
				kind.geburtsdatum = parts[2];
				kind.bravheitsgrad = parts[3];
				String ort="";
				for (int i = 4; i < parts.length; i++)
					kind.ort += parts[i] + " ";
				kinder[counter] = kind;
				 
				counter++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
				}
		}
		return kinder;

	}

}