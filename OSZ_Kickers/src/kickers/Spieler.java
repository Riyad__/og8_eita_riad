package kickers;

import java.util.Arrays;

public class Spieler extends Mitglieder {
	private Mannschaft[] mannschft;
	private int trikotnummer;
	private String spielposition;

	public Spieler() {
	}

	public Spieler(int trikotnummer, String spielposition) {
		super();
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
		this.mannschft = new Mannschaft[1];
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public Mannschaft[] getMannschft() {
		return mannschft;
	}

	public void setMannschft(Mannschaft[] mannschft) {
		this.mannschft = mannschft;
	}

	@Override
	public String toString() {
		return "Spieler [mannschft=" + Arrays.toString(mannschft) + ", trikotnummer=" + trikotnummer
				+ ", spielposition=" + spielposition + "]";
	}

}
