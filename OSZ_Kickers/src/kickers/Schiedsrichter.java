package kickers;

public class Schiedsrichter extends Mitglieder {
	private int anzgepfiffenenSpieler;

	public Schiedsrichter(int anzgepfiffenenSpieler) {
		super();
		this.anzgepfiffenenSpieler = anzgepfiffenenSpieler;
	}

	public int getAnzgepfiffenenSpieler() {
		return anzgepfiffenenSpieler;
	}

	public void setAnzgepfiffenenSpieler(int anzgepfiffenenSpieler) {
		this.anzgepfiffenenSpieler = anzgepfiffenenSpieler;
	}

}
