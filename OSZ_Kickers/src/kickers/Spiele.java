package kickers;

import java.util.Arrays;

public class Spiele {

	// Anfang Attribute
	private Mannschaft[] mannschaft;
	private String spieltyp;
	private String spielDatum;
	private String ergebnis;
	// Ende Attribute

	// Anfang Methoden
	public Spiele(String spieltyp, String spielDatum, String ergebnis) {
		this.spieltyp = spieltyp;
		this.spielDatum = spielDatum;
		this.ergebnis = ergebnis;
		this.mannschaft = new Mannschaft[2];
	}

	public String getSpieltyp() {
		return spieltyp;
	}

	public void setSpieltyp(String spieltypNeu) {
		spieltyp = spieltypNeu;
	}

	public String getSpielDatum() {
		return spielDatum;
	}

	public void setSpielDatum(String spielDatumNeu) {
		spielDatum = spielDatumNeu;
	}

	public String getErgebnis() {
		return ergebnis;
	}

	public void setErgebnis(String ergebnisNeu) {
		ergebnis = ergebnisNeu;
	}

	public Mannschaft[] getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft[] mannschaft) {
		this.mannschaft = mannschaft;
	}

	@Override
	public String toString() {
		return "Spiele [mannschaft=" + Arrays.toString(mannschaft) + ", spieltyp=" + spieltyp + ", spielDatum="
				+ spielDatum + ", ergebnis=" + ergebnis + "]";
	}

	// Ende Methoden
} // end of Spiele
