package kickers;

public class Mitglieder {
	// Attribute
	private String name;
	private long telefonnummer;
	private boolean istJahresbeitrag;

	// Konstruktoren
	public Mitglieder() {

	}

	public Mitglieder(String name, long telefonnummer, boolean istJahresbeitrag) {
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.istJahresbeitrag = istJahresbeitrag;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(long telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isIstJahresbeitrag() {
		return istJahresbeitrag;
	}

	public void setIstJahresbeitrag(boolean istJahresbeitrag) {
		this.istJahresbeitrag = istJahresbeitrag;
	}

}
