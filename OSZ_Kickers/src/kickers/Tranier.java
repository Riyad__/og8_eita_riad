package kickers;

import java.util.Arrays;

public class Tranier extends Mitglieder {
	private Mannschaft[] mannschaft;
	private char lizenzKlasse;
	private double aufwandschaedigung;

	public Tranier() {

	}

	public Tranier(char lizenzKlasse, double aufwandschaedigung) {
		super();
		this.lizenzKlasse = lizenzKlasse;
		this.aufwandschaedigung = aufwandschaedigung;
		this.mannschaft = new Mannschaft[1];
	}

	public char getLizenzKlasse() {
		return lizenzKlasse;
	}

	public void setLizenzKlasse(char lizenzKlasse) {
		this.lizenzKlasse = lizenzKlasse;
	}

	public double getAufwandschaedigung() {
		return aufwandschaedigung;
	}

	public void setAufwandschaedigung(double aufwandschaedigung) {
		this.aufwandschaedigung = aufwandschaedigung;
	}

	public Mannschaft[] getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft[] mannschaft) {
		this.mannschaft = mannschaft;
	}

	@Override
	public String toString() {
		return "Tranier [mannschaft=" + Arrays.toString(mannschaft) + ", lizenzKlasse=" + lizenzKlasse
				+ ", aufwandschaedigung=" + aufwandschaedigung + "]";
	}

}
