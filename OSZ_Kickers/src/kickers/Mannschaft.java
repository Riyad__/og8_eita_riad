package kickers;

import java.util.Arrays;

public class Mannschaft {

	// Anfang Attribute
	private int anzSpiele;
	private Spieler[] spieler;
	private Tranier tranier;
	private String name;
	private String spielKlasse;
	// Ende Attribute

	// Anfang Methoden

	public String getName() {
		return name;
	}

	public Mannschaft() {

	}

	public Mannschaft(int anzSpiele, Tranier tranier, String name, String spielKlasse) {
		super();
		this.anzSpiele = anzSpiele;
		this.spieler = new Spieler[22];
		this.setTranier(tranier);
		this.name = name;
		this.spielKlasse = spielKlasse;

	}

	public void setName(String nameNeu) {
		name = nameNeu;
	}

	public String getSpielKlasse() {
		return spielKlasse;
	}

	public void setSpielKlasse(String spielKlasseNeu) {
		spielKlasse = spielKlasseNeu;
	}

	public Tranier getTranier() {
		return tranier;
	}

	public void setTranier(Tranier tranier) {
		this.tranier = tranier;
	}

	public Spieler[] getSpieler() {
		return spieler;
	}

	public void setSpieler(Spieler[] spieler) {
		this.spieler = spieler;
	}

	public int getAnzSpiele() {
		return anzSpiele;
	}

	public void setAnzSpiele(int anzSpiele) {
		this.anzSpiele = anzSpiele;
		anzSpiele++;
	}

	@Override
	public String toString() {
		return "Mannschaft [anzSpiele=" + anzSpiele + ", spieler=" + Arrays.toString(spieler) + ", tranier=" + tranier
				+ ", name=" + name + ", spielKlasse=" + spielKlasse + "]";
	}

	// Ende Methoden
} // end of Mannschaft
