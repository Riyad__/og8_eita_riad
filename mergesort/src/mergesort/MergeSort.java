package mergesort;

import java.util.Arrays;

public class MergeSort {

    public static void main(String[] args) {
       java.util.Random r = new java.util.Random();
       int a[] = new int[20]; 
        for(int z = 0; z <20 ; z++) {
          a[z] = (r.nextInt(40));
          
        }
        System.out.println("MergeSort:");
        System.out.println("__________");
        System.out.println("");
        System.out.println("Start:");
        System.out.println("");
        for (int i = 0; i < a.length; i++) {
        
            System.out.print(" "+a[i]);
        }
        System.out.println();
        System.out.println("");
        mergeSort(a, a.length);
        System.out.println("Ende:");
        System.out.println("");
        for (int i = 0; i < a.length; i++)
          
            System.out.print(" "+a[i]);
    }

    public static void mergeSort(int[] a, int n) {
    
      System.out.println(Arrays.toString(a));
    
        if (n < 2)
            return;
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
            
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, n - mid);

        merge(a, l, r, mid, n - mid);
       System.out.println("");
    }

    public static void merge(int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;

        while (i < left && j < right) {

            if (l[i] <= r[j])
                a[k++] = l[i++];
            else
                a[k++] = r[j++];

        }

        while (i < left)
            a[k++] = l[i++];

        while (j < right)
            a[k++] = r[j++];
    }
}
